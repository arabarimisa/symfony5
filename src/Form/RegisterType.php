<?php

namespace App\Form;

use App\Entity\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => "Votre nom",
                'attr' => [
                    'placeholder' => 'Veuiller tapez votre nom'
                ]
            ])
            ->add('prenom', TextType::class, [
                'label' => "Votre prenom",
                'attr' => [
                    'placeholder' => 'Veuiller tapez votre prenom'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => "Adresse Email",                
                'attr' =>[
                    'placeholder' => 'Veuiller tapez votre email'
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => "Les mot de passe doit identique",
                'required' => true,
                'first_options' => [
                    'label' => "Mot de passe", 
                    'attr'=>[
                        'placeholder'=> "Votre mot de passe "
                        ]
                    ],
                'second_options' => [
                    'label' => "Confirmer le mot de passe", 
                    'attr'=> [
                        'placeholder'=> "Confirmer votre mot de passe"
                        ]
                    ]                
            ])
            ->add('roles', ChoiceType::class, [
                'label' => "Role",
                'attr' => [
                    'class'=>'form-control myselect',
                    'placeholder' => "Choisir votre role"
                ],              
                'choices' => [
                    'Utilisateur' => "ROLE_USER",
                    'Administrateur' => "ROLE_ADMIN",
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => "S'inscrire",
                'attr' =>[
                    'class' => "btn btn-success"
                ]
                
            ])        
        ;
        $builder
                ->get('roles')
                ->addModelTransformer(new CallbackTransformer(
                    function ($rolesAsArray) {
                        return implode(',', $rolesAsArray);
                    },
                    function ($rolesAsString) {
                        return explode(',', $rolesAsString);
                    }
                ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
