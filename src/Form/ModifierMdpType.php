<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ModifierMdpType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'disabled' => true, 'label' => 'Mon adresse email'
                ])
            ->add('nom', TextType::class, [ 
                'disabled' => true, 'label' => "Votre nom"
                ])
            ->add('prenom', TextType::class, [ 
                'disabled' => true, 'label' => "Votre prenom"
                ])
            ->add('old_password', PasswordType::class, [
                'label' => "Votre mot de passe actuel", 
                'mapped' => false,
                'attr'=> [
                    'placeholder' => "Veuillez saisir votre mot de passe actuel"]
                    ])

            ->add('new_password', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => false,
                'invalid_message' => "Les mot de passe doit identique",
                'required' => true,
                'first_options' => ['label' => "Nouveau mot de passe", 'attr'=>['placeholder'=> "Votre nouveau mot de passe "]],
                'second_options' => ['label' => "Confirmer votre nouveau mot de passe", 'attr'=> ['placeholder'=> "Confirmer votre nouveau mot de passe"]]
                
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Mettre a jour",
                
                'attr' =>[
                    'class' => "btn btn-success"
                ]
                
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
