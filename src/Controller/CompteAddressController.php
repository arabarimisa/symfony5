<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Address;
use App\Form\AddressType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompteAddressController extends AbstractController
{
    private $entityManageer;

    public function __construct(EntityManagerInterface $entityManageer)
    {
        $this->entityManageer = $entityManageer;
    }
    
    /**
     * @Route("/compte/adresses", name="app_compte_address")
     */
    public function index(): Response //Voir tout les adresse dejat ajouter
    {
        return $this->render('compte/address.html.twig');
    }

    /**
     * @Route("/compte/ajout-une-adresse", name="app_compte_address_add")
     */

     //ajouter une adresse
    public function add( Cart $cart,  Request $request): Response //Créer un form et ajouter une adresse
    {
        $address = new Address();

        $form = $this->createForm(AddressType::class, $address);

        //requette pour enregistrer
        $form->handleRequest($request);

        //enregistrez dans la base de données
        if($form->isSubmitted() && $form->isValid() )
        {
            $address->setUser($this->getUser());
            $this->entityManageer->persist($address); //Enregistrer dans le base de donnée
            $this->entityManageer->flush();
            
            if($cart->get()){
                return $this->redirectToRoute('cart');
            }
            else
            {
                return $this->redirectToRoute('app_compte_address');
            }
        }    

        return $this->render('compte/address_add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/compte/modifier-une-adresse/{id}", name="app_compte_address_edit")
     */

    public function edit(Request $request, $id): Response //Modifier une adresse et prend les formulaire d'ajout une adresse
    {
        $address = $this->entityManageer->getRepository(Address::class)->findOneById($id); //Prend l'adresse à modifier

        if(!$address || $address->getUser() != $this->getUser()) //(securite) condition si l'adresse modifier n'est pas à celui de l'user
        {
            return $this->redirectToRoute('app_compte_address');
        }

        $form = $this->createForm(AddressType::class, $address); 

        //requette pour enregistrer
        $form->handleRequest($request);

        //enregistrez dans le base données
        if($form->isSubmitted() && $form->isValid() )
        {
            $this->entityManageer->flush();

            return $this->redirectToRoute('app_compte_address');
        }    

        return $this->render('compte/address_add.html.twig', [
            'form' => $form->createView()
        ]);
    }

     /**
     * @Route("/compte/supprimer-une-adresse/{id}", name="app_compte_address_delete")
     */
    
    public function delete($id): Response //Effacer une adresse
    {
        $address = $this->entityManageer->getRepository(Address::class)->findOneById($id);

        if($address && $address->getUser() == $this->getUser())
        {
            $this->entityManageer->remove($address);
            $this->entityManageer->flush();
        }

        return $this->redirectToRoute('app_compte_address');
    }
}
