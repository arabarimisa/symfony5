<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ModifierMdpType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CompteMdpController extends AbstractController
{
    private  $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/compte/modifier-mon-mot-de-passe", name="app_compte_mdp")
     */
    public function index(Request $request, UserPasswordHasherInterface  $hasher): Response
    {
        $notification = null;

        //Creation du formulaire 
        $user = $this->getUser();//prend l'utilisateur courant
        $form = $this->createForm( ModifierMdpType::class, $user);

        //requete pour enregistrer le nouveau mdp
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() )
        {
            //Commencer a modifier le mot de passe
            $old_pwd = $form->get('old_password')->getData(); //Prend le donnée sur le formulaire old_password 

            if($hasher->isPasswordValid($user, $old_pwd))
            {
                //encoder le mot de passe
              $new_pwd = $form->get('new_password')->getData(); //Prend le donnée sur le formulaire new_password 
              
              $password = $hasher->hashPassword($user, $new_pwd);

              //instacier 
              if($user instanceof User){
                $user->setPassword($password);
              }

            //Enregistrer dans le BD
            $this->entityManager->flush();
            $notification = "Votre mot de passe a bien été mis a jour";
            return $this->redirectToRoute('app_logout');
           
            } else
            {
                $notification ="Votre mot de passe actuel n'est pas le bon ";
            }
        }

        return $this->render('compte/mdp.html.twig', [
            //creation du formulaire dans le twig
            'form' => $form->createView(),
            'notification' => $notification
        ]);
    }
}
