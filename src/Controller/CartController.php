<?php

namespace App\Controller;

use App\Classe\Cart;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{

    /**
     * @Route("/mon-panier", name="cart")
     */

    //Voir le panier
    public function index(Cart $cart): Response
    {
        //Si il n'y a pas d'user redirect à mes produits
        if(!$this->getUser()){
            return $this->redirectToRoute('app_products');
        }

        return $this->render('cart/index.html.twig', [
            'cart' => $cart->getFull()
        ]);
    }

    /**
     * @Route("/cart/add/{id}", name="add_to_cart")
     */

     //Ajouter dans le panier
    public function add(Cart $cart, $id): Response
    {
        $cart->add($id);
        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/remove", name="remove_my_cart")
     */

    //Vider le panier
    public function remove(Cart $cart): Response
    {
        $cart->remove();
        return $this->redirectToRoute('cart');
    }

     /**
     * @Route("/cart/delete/{id}", name="delete_to_cart")
     */

    //Effacer un produit
    public function delete(Cart $cart, $id): Response
    {
        $cart->delete($id);
        return $this->redirectToRoute('cart');
    }
    
     /**
     * @Route("/cart/decrease/{id}}", name="decrease_to_cart")
     */

    //diminuer un produit
    public function decrease(Cart $cart, $id): Response
    {
        $cart->decrease($id);
        return $this->redirectToRoute('cart');
    }
    
}
