<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegisterController extends AbstractController
{
    //initialisation de variable doctrine (pour enregistrer dans bd)
    private  $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/inscription", name="app_register")
     */
    public function index(Request $request,UserPasswordHasherInterface  $encoder): Response 
    {
        //Creation des formulaire
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);

        //Requete POST pour l'enregistrement
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) 
        { 
            //Prend la valeur dans les formulaire
            $user = $form->getData();

            //hasher le mot de passe
            $password = $encoder->hashPassword($user, $user->getPassword());
            
            if($user instanceof User)
            {
                 $user->setPassword($password);
            }

            //Enregistrer dans le BD
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_login');            
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
