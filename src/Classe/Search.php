<?php

namespace App\Classe;

use App\Entity\Categorie;

class Search 
{
    /**
     * 
     * @var string
     */
    public $recherche = '';

    /**
     *
     * @var Categorie[]
     */
    public $categories = [];
}