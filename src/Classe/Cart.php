<?php

namespace App\Classe;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Cart
{
    private $session;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }
    
    public function add($id) 
    {
        $cart = $this->session->get('cart', []);
        
        if(!empty($cart[$id])){
            $cart[$id]++;
        }else{
            $cart[$id] = 1;
        }

        $this->session->set('cart', $cart);
    }

    public function get()
    {
        return $this->session->get('cart');
    }

    public function remove()
    {
        return $this->session->remove('cart');
    }

    public function delete($id)
    {
        $cart = $this->session->get('cart', []);

        unset($cart[$id]);

        return $this->session->set('cart', $cart);
    }

    public function decrease($id)
    {
        //Recuperer la cart
        $cart = $this->session->get('cart', []);

        if($cart[$id] > 1){
            //retirer une quantite
            $cart[$id]--;

        } else{
            //supprimer mon produit
            unset($cart[$id]);
        }

        return $this->session->set('cart', $cart);
    }

    //prend les valeurs ajouter dans le panier
    public function getFull() 
    {        
        $cartComplete = [];

    if($this->get()){
        foreach($this->get() as $id => $quantity)
        {
            //ne pas ajouter dans le panier l'id inexiste
            $product_object = $this->entityManager->getRepository(Product::class)->findOneById($id);

            if(!$product_object){
                $this->delete($id);
                continue;
            }

            $cartComplete[] = [
                'product' => $product_object,
                'quantity' => $quantity
            ];
        } 
      }
      return $cartComplete;
    }
}